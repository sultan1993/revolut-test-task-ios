//
//  UIViewExtensions.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/7/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    class var identifier: String {
        return String(describing: self)
    }
    
    var identifier: String {
        return String(describing: type(of: self))
    }
    
    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}
