//
//  ApiCurrency.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

struct APICurrency: Codable {
    var base: String
    var date: String
    var rates: [String: Float]
}
