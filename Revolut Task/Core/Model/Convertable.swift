//
//  Convertable.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

protocol Convertable {
    func convert(originCurrency: Currency?, amount: Float)
}
