//
//  NetworkLogger.swift
//  Sultan
//
//  Created by Sultan on 4/24/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

struct NetworkLogger {
    
    static func log(request: URLRequest) {

        print("\n - - - - - - - - - - OUTGOING REQUEST - - - - - - - - - - \n")
        defer { print("\n - - - - - - - - - -  END OF REQUEST - - - - - - - - - - \n") }

        let urlAsString = request.url?.absoluteString ?? ""
        let urlComponents = NSURLComponents(string: urlAsString)

        let method = request.httpMethod != nil ? "\(request.httpMethod ?? "")" : ""
        let path = "\(urlComponents?.path ?? "")"
        let query = "\(urlComponents?.query ?? "")"
        let host = "\(urlComponents?.host ?? "")"

        var logOutput = """
                        \(urlAsString) \n
                        \(method) \(path)?\(query) HTTP/1.1 \n
                        HOST: \(host)\n
                        """

        for (key, value) in request.allHTTPHeaderFields ?? [:] {
            logOutput += "\(key): \(value) \n"
        }

        if let body = request.httpBody {
            logOutput += "\n\(String(data: body, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/") ?? "")"

            if let object = try? JSONSerialization.jsonObject(with: body, options: []),
               let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]) {

                logOutput += "\n\(String(data: data, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/") ?? "")"
            }
        }

        print(logOutput)
    }

    static func log(response: URLResponse?, responseData: Data?, error: Error?) {
        print("\n - - - - - - - - - - INCOMING RESPONSE - - - - - - - - - - \n")
        defer { print("\n - - - - - - - - - -  END OF RESPONSE - - - - - - - - - - \n") }

        var logOutput = ""

        if let response = response as? HTTPURLResponse {
            logOutput += "\nStatus code: \(response.statusCode) \n"
        }

        if let responseData = responseData,
            let object = try? JSONSerialization.jsonObject(with: responseData, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]) {

            logOutput += "\n\(String(data: data, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/") ?? "") \n"
        }

        if let error = error {
            logOutput += "\n \(error.localizedDescription)"
        }

        print(logOutput)
    }
}
