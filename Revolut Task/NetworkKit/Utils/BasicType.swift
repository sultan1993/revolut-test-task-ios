//
//  BasicType.swift
//  Sultan
//
//  Created by Sultan on 7/11/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public enum BasicType: Decodable {
    case string(String)
    case bool(Bool)
    case int(Int)
    case double(Double)

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        if let string = try? container.decode(String.self) {
            self = .string(string)
        } else if let bool = try? container.decode(Bool.self) {
            self = .bool(bool)
        } else if let int = try? container.decode(Int.self) {
            self = .int(int)
        } else if let double = try? container.decode(Double.self) {
            self = .double(double)
        } else {
            throw DecodingError.typeMismatch(BasicType.self, DecodingError.Context(codingPath: decoder.codingPath,
                                                                                   debugDescription: "Unsupported JSON type"))
        }
    }
}
