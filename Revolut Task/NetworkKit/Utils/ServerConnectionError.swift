//
//  ServerConnectionError.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public enum ServerConnectionError: Error {
    case custom(message: String)
    case encodingFailed
    case requestFailed
    case missingURL
    case httpError(code: Int)
    case noInternetAccess
}

extension ServerConnectionError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .custom(let message):
            return message
        case .encodingFailed:
            return "Parameters encoding failed"
        case .requestFailed:
            return "Request processing failed"
        case .missingURL:
            return "URL is nil"
        case .httpError(let code):
            return "HTTP Error \(code)"
        case .noInternetAccess:
            return "No internet connection"
        }
    }
}
