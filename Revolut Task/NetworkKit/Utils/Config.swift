//
//  Config.swift
//  NetworkKit
//
//  Created by Sultan on 7/26/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public struct Config {

    public static var appVersion: String?

    private init() {}
    
    static var userToken: String?
    static var consumerToken: String?

    public static func setConsumerToken(_ token: String?) {
        consumerToken = token
    }

    public static func setUserToken(_ token: String?) {
        userToken = token
    }
}
