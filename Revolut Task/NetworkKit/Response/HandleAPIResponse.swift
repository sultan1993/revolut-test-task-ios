//
//  HandleAPIResponse.swift
//  Sultan
//
//  Created by Sultan on 5/24/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public protocol HandleAPIResponse {
    func response<T: Decodable>(_ data: Data,
                                success: @escaping (T) -> Void,
                                failure: @escaping (Error) -> Void)

    func response(_ data: Data,
                  success: @escaping ([String: Any]) -> Void,
                  failure: @escaping (Error) -> Void)
}

public extension HandleAPIResponse {
    func response<T: Decodable>(_ data: Data,
                                success: @escaping (T) -> Void,
                                failure: @escaping (Error) -> Void) {
        do {
            validateForErrors(data, failure: failure)

            let decoder = JSONDecoder()

            if let keyCodingStrategy = CodingStrategy.key {
                decoder.keyDecodingStrategy = keyCodingStrategy
            }

            if let dateCodingStrategy = CodingStrategy.date {
                decoder.dateDecodingStrategy = dateCodingStrategy
            }

            let decodedResponse = try decoder.decode(T.self, from: data)
            success(decodedResponse)
        } catch {
            failure(error)
        }
    }

    func response(_ data: Data,
                  success: @escaping ([String: Any]) -> Void,
                  failure: @escaping (Error) -> Void) {

        validateForErrors(data, failure: failure)

        do {
            guard let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else { return }
            success(dictionary)
        } catch {
            failure(error)
        }
    }

    private func validateForErrors(_ data: Data, failure: (Error) -> Void) {
        do {
            guard
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                let errors = json["errors"] as? [[String: String]] else { return }

            var errorMessage = ""
            errors.forEach { errorMessage += ($0["message"] ?? "") + "\n" }
            failure(ServerConnectionError.custom(message: errorMessage))
        } catch {
            failure(error)
        }
    }
}
