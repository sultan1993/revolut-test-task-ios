//
//  APIResult.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public enum APIResult<T> {
    case success(T)
    case failure(Error)
}
