//
//  EndPointType.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum AuthorizationLevel {
    case basic
    case user
    case consumer
}

public enum BodyType {
    case json
    case formData
    case none
}

public protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var requestURL: URL { get }
    var authLevel: AuthorizationLevel { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var bodyParameters: Parameters? { get }
    var urlParameters: Parameters? { get }
    var bodyType: BodyType { get }
    var isPlatformRequest: Bool { get }
}

public extension EndPointType {
    
    var requestURL: URL {
        return baseURL.appendingPathComponent(path)
    }

    var authLevel: AuthorizationLevel {
        return .basic
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var headers: HTTPHeaders? {
        return nil
    }

    var bodyParameters: Parameters? {
        return nil
    }

    var urlParameters: Parameters? {
        return nil
    }

    var bodyType: BodyType {
        return .json
    }

    var isPlatformRequest: Bool {
        return false
    }
}
