//
//  APIRequest.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

struct APIRequest {

    func buildRequest(from endPoint: EndPointType) throws -> URLRequest {
        var request = URLRequest(url: endPoint.requestURL,
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)

        request.httpMethod = endPoint.httpMethod.rawValue

        addAditionalHeaders(constructHeaders(for: endPoint), request: &request)

        do {
            try self.configureParameters(for: endPoint, request: &request)
            return request
        } catch {
            throw error
        }
    }

    private func addAditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }

        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }

    private func constructHeaders(for endPoint: EndPointType) -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "x-app-platform": "ios",
            "x-app-version": Config.appVersion ?? "",
            "platform": "ios",
            "app_version": Config.appVersion ?? "",
            "Accept-Language": Locale.preferredLanguages.first ?? ""
        ]

        if endPoint.authLevel == .user {
            headers["Authorization"] = "Bearer \(Config.userToken ?? "")"
        }

        if endPoint.authLevel == .consumer {
            headers["Authorization"] = "Bearer \(Config.consumerToken ?? "")"
        }

        if let additionalHeaders = endPoint.headers, !additionalHeaders.isEmpty {
            additionalHeaders.forEach { headers[$0.key] = $0.value }
        }

        return headers
    }

    private func configureParameters(for endPoint: EndPointType, request: inout URLRequest) throws {
        do {
            let bodyParameters = constructParameters(for: endPoint)

            if let urlParameters = endPoint.urlParameters {
                try URLParameterEncoder.encode(urlRequest: &request, with: urlParameters)
            }

            if endPoint.httpMethod == .get {
                return
            }

            if endPoint.bodyType == .json {
                try JSONParameterEncoder.encode(urlRequest: &request, with: bodyParameters)
            } else {
                try FormDataParameterEncoder.encode(urlRequest: &request, with: bodyParameters)
            }
        } catch {
            throw error
        }
    }

    private func constructParameters(for endPoint: EndPointType) -> Parameters {
        var params: Parameters = [:]

        if !endPoint.isPlatformRequest {
            params = [
                "platform": "ios",
                "app_version": Config.appVersion ?? ""
            ]
        }

        if let additionalParameters = endPoint.bodyParameters, !additionalParameters.isEmpty {
            additionalParameters.forEach { params[$0.key] = $0.value }
        }

        return params
    }
}
