//
//  NetworkRequestHandler.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public protocol NetworkRequestHandler: class {
    associatedtype EndPoint: EndPointType

    func request<T: Decodable>(_ endPoint: EndPoint,
                               responseType: T.Type,
                               success: @escaping (T) -> Void,
                               failure: @escaping (Error) -> Void)
    
    func request(_ endPoint: EndPoint,
                success: @escaping () -> Void,
                failure: @escaping (Error) -> Void)

    func request(_ endPoint: EndPoint,
                 success: @escaping ([String: Any]) -> Void,
                 failure: @escaping (Error) -> Void)

    func cancel()
}
