//
//  HTTPMethod.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
