//
//  APIClient.swift
//  Sultan
//
//  Created by Sultan on 5/24/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public struct CodingStrategy {
    static var date: JSONDecoder.DateDecodingStrategy?
    static var key: JSONDecoder.KeyDecodingStrategy?
}

public class APIClient<EndPoint: EndPointType>: NetworkRequestHandler, HandleAPIResponse {

    public init() {}

    public var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase {
        didSet {
            CodingStrategy.key = keyDecodingStrategy
        }
    }

    public var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate {
        didSet {
            CodingStrategy.date = dateDecodingStrategy
        }
    }

    private var task: URLSessionTask?
    private let session = URLSession(configuration: .default)
    private let apiRequest = APIRequest()
    private let reachability = try! Reachability(hostname: "google.com")

    public func request<T: Decodable>(_ endPoint: EndPoint,
                                      responseType: T.Type,
                                      success: @escaping (T) -> Void,
                                      failure: @escaping (Error) -> Void) {
        do {
            let request = try apiRequest.buildRequest(from: endPoint)

            task = session.dataTask(with: request) { [unowned self] data, response, error in
                DispatchQueue.main.async {
                    NetworkLogger.log(request: request)
                    NetworkLogger.log(response: response, responseData: data, error: error)

                    self.validateForErrors(error: error, response: response, data: data, failure: failure)
                }

                guard let data = data else { return }

                DispatchQueue.main.async {
                    self.response(data, success: success, failure: failure)
                }
            }
        } catch {
            failure(error)
        }

        self.task?.resume()
    }

    public func request(_ endPoint: EndPoint,
                        success: @escaping () -> Void,
                        failure: @escaping (Error) -> Void) {
        do {
            let request = try apiRequest.buildRequest(from: endPoint)

            task = session.dataTask(with: request) { [unowned self] data, response, error in
                DispatchQueue.main.async {
                    NetworkLogger.log(request: request)
                    NetworkLogger.log(response: response, responseData: data, error: error)

                    self.validateForErrors(error: error, response: response, data: data, failure: failure)
                    success()
                }
            }
        } catch {
            failure(error)
        }

        self.task?.resume()
    }

    public func request(_ endPoint: EndPoint,
                        success: @escaping ([String: Any]) -> Void,
                        failure: @escaping (Error) -> Void) {
        do {
            let request = try apiRequest.buildRequest(from: endPoint)

            task = session.dataTask(with: request) { [unowned self] data, response, error in
                DispatchQueue.main.async {
                    NetworkLogger.log(request: request)
                    NetworkLogger.log(response: response, responseData: data, error: error)

                    self.validateForErrors(error: error, response: response, data: data, failure: failure)
                }

                guard let data = data else { return }

                DispatchQueue.main.async {
                    self.response(data, success: success, failure: failure)
                }
            }
        } catch {
            failure(error)
        }

        self.task?.resume()
    }

    private func validateForErrors(error: Error?,
                                   response: URLResponse?,
                                   data: Data?,
                                   failure: @escaping (Error) -> Void) {

        if reachability.connection == .unavailable {
            failure(ServerConnectionError.noInternetAccess)
            return
        }

        if let error = error {
            failure(error)
            return
        }

        if let httpResponse = response as? HTTPURLResponse, !(200...299).contains(httpResponse.statusCode) {
            failure(ServerConnectionError.httpError(code: httpResponse.statusCode))
            return
        }

        if data == nil {
            failure(ServerConnectionError.requestFailed)
            return
        }
    }

    public func cancel() {
        self.task?.cancel()
    }
}
