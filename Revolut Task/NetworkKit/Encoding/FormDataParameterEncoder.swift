//
//  FormDataParameterEncoder.swift
//  NetworkKit
//
//  Created by Sultan on 8/8/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

class FormDataParameterEncoder: ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        var formData: [String] = []
        
        for (key, value) in parameters {
            if let array = value as? [String] {
                array.forEach {
                    formData.append(key + "=\($0)")
                }
            } else {
                formData.append(key + "=\(value)")
            }
        }
        
        if urlRequest.httpMethod != "GET" {
            urlRequest.httpBody = formData.map { String($0) }
                .joined(separator: "&")
                .data(using: .utf8)
        }
        
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
    }
}
