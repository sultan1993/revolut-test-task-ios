//
//  ParameterEncoder.swift
//  Sultan
//
//  Created by Sultan on 4/23/19.
//  Copyright © 2019 Sultan. All rights reserved.
//

import Foundation

public typealias Parameters = [String: Any]

protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}
