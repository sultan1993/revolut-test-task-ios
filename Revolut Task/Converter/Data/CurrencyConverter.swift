//
//  CurrencyConverter.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/7/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

protocol CurrencyConverter {
    
    var selectedCurrency: Currency? { get set }
    
    var enteredAmount: Float? { get set }
    
    func convert(list: [Convertable], currency: Currency?, amount: Float?)
}

class CurrencyConverterImpl: CurrencyConverter {
    
    private enum Constant {
        static let defaultAmount: Float = 1.0
    }
    
    var selectedCurrency: Currency?
    
    var enteredAmount: Float?
    
    func convert(list: [Convertable], currency: Currency? = nil, amount: Float? = nil) {
        if let amount = amount {
            enteredAmount = amount
        }
        
        if let currency = currency {
            selectedCurrency = currency
        }
        
        list.forEach {
            $0.convert(originCurrency: selectedCurrency, amount: enteredAmount ?? Constant.defaultAmount)
        }
    }
}
