//
//  CurrencyAPIEndpoint.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

enum CurrencyAPIEndpoint: EndPointType {
    
    case fetchRates(base: String)
    
    var baseURL: URL {
        return URL(string: "https://revolut.duckdns.org/")!
    }
    
    var path: String {
        return "latest"
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var urlParameters: Parameters? {
        switch self {
        case .fetchRates(let base):
            return ["base" : base]
        }
    }
}
