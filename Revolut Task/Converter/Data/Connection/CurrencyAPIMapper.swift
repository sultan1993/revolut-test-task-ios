//
//  CurrencyAPIMapper.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/7/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

class CurrencyAPIMapper {
    
    static func map(apiCurrency: APICurrency) -> [Currency] {
        var currencyList = [Currency]()
        
        let baseCurrency = Currency(code: apiCurrency.base, rate: 1.0, isBase: true)
        currencyList.append(baseCurrency)
        
        
        let sortedKeysAndValues = apiCurrency.rates.sorted(by: { $0.0 < $1.0 })
        for (key, value) in sortedKeysAndValues {
            let currency = map(key: key, value: value)
            currencyList.append(currency)
        }
        
        return currencyList
    }
    
    private static func map(key: String, value: Float) -> Currency {
        return Currency(code: key, rate: value, isBase: false)
    }
    
}
