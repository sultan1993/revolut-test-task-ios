//
//  CurrencyRepository.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

protocol CurrencyRepository {
    func fetchRates(base: String, success: @escaping ([Currency]) -> Void, failure: @escaping (Error) -> Void)
}

class CurrencyAPIRepository: CurrencyRepository {
    
    private lazy var apiClient: APIClient<CurrencyAPIEndpoint> = {
        let client = APIClient<CurrencyAPIEndpoint>()
        client.keyDecodingStrategy = .useDefaultKeys
        return client
    }()
    
    func fetchRates(base: String, success: @escaping ([Currency]) -> Void, failure: @escaping (Error) -> Void) {
        apiClient.request(.fetchRates(base: base), responseType: APICurrency.self, success: { response in
            success(CurrencyAPIMapper.map(apiCurrency: response))
        }, failure: { error in
            failure(error)
        })
    }
}
