//
//  Currency.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

class Currency: Codable {
    
    var code: String
    var rate: Float
    var isBase: Bool
    var convertedAmount: Float
    
    init(code: String, rate: Float, isBase: Bool) {
        self.code = code
        self.rate = rate
        self.isBase = isBase
        self.convertedAmount = rate
    }
}

extension Currency: Convertable {
    
    func convert(originCurrency: Currency?, amount: Float) {
        if let currency = originCurrency {
            if currency.isBase && self.isBase {
                convertedAmount = amount * rate
            } else {
                if self.isBase {
                    convertedAmount = amount / currency.rate
                } else {
                    convertedAmount = (amount / currency.rate) * rate
                }
            }
        } else {
            convertedAmount = amount * rate
        }
    }
}
