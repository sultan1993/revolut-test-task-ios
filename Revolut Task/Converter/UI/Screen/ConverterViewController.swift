//
//  ViewController.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import UIKit

class ConverterViewController: UIViewController {
    
    private var tableView = UITableView()
    private var viewModel: ConverterViewModel
    
    init(viewModel: ConverterViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Revolut Rates"
        setupViews()
        setupViewModel()
    }
    
    private func setupViews() {
        let safeArea = view.safeAreaLayoutGuide
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ConverterItemCell.nib, forCellReuseIdentifier: ConverterItemCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.reloadData()
        
        view.addSubview(tableView)
        view.backgroundColor = .white
        
        tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 0.0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0.0).isActive = true
        tableView.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 0.0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 0.0).isActive = true
    }
    
    private func setupViewModel() {
        viewModel.ratesFetched = { [unowned self] in
            self.tableView.reloadData()
        }
        
        viewModel.itemSelected = { [unowned self] position in
            let currentIndexPath = IndexPath(row: position, section: 0)
            let firstIndexPath = IndexPath(row: 0, section: 0)
            self.tableView.moveRow(at: currentIndexPath, to: firstIndexPath)
            self.tableView.scrollToRow(at: firstIndexPath, at: .top, animated: true)
        }
        
        viewModel.ratesUpdated = { [unowned self] in
            guard var visibleIndexPaths = self.tableView.indexPathsForVisibleRows else { return }
            
            //remove selected row from updates
            if let _ = self.tableView.indexPathForSelectedRow {
                visibleIndexPaths.remove(at: 0)
            }
                
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: visibleIndexPaths, with: .none)
            self.tableView.endUpdates()
        }
        
        viewModel.startFetchingRates()
    }
}

extension ConverterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectItem(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ConverterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.currencyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConverterItemCell.identifier) as! ConverterItemCell
        cell.configure(with: viewModel.currencyList[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension ConverterViewController: ConverterCellDelegate {
    
    func converterCell(_ cell: ConverterItemCell, didChangeCurrency currency: Currency, with amount: Float) {
        viewModel.amountChanged(currency: currency, amount: amount)
    }
}
