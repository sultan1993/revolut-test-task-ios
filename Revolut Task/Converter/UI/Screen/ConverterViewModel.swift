//
//  ConverterViewModel.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/6/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import Foundation

class ConverterViewModel {
    
    private enum Constant {
        static let base = "EUR"
        static let delay: TimeInterval = 1.0
    }
    
    private var repository: CurrencyRepository
    private var converter: CurrencyConverter
    private var fetchTimer: Timer?
    
    var currencyList: [Currency] = []
    var ratesFetched: (() -> Void)?
    var ratesUpdated: (() -> Void)?
    var itemSelected: ((Int) -> Void)?
    
    init(repository: CurrencyRepository, converter: CurrencyConverter) {
        self.repository = repository
        self.converter = converter
    }
    
    func startFetchingRates() {
        if fetchTimer != nil {
            return
        }
        
        fetchTimer = Timer.scheduledTimer(timeInterval: Constant.delay,
                                          target: self,
                                          selector: #selector(fetchRates),
                                          userInfo: nil,
                                          repeats: true)
    }
    
    @objc private func fetchRates() {
        repository.fetchRates(base: Constant.base, success: { list in
            let isUpdate = !self.currencyList.isEmpty
            
            self.refreshList(fetchedList: list)
            self.converter.convert(list: self.currencyList,
                                   currency: nil,
                                   amount: nil)
            
            if isUpdate {
                self.ratesUpdated?()
            } else {
                self.ratesFetched?()
            }
        }, failure: { _ in })
    }
    
    func amountChanged(currency: Currency, amount: Float) {
        converter.convert(list: currencyList, currency: currency, amount: amount)
        ratesUpdated?()
    }
    
    private func refreshList(fetchedList: [Currency]) {
        if currencyList.isEmpty {
            currencyList = fetchedList
        } else {
            //update existing list's rates
            for i in currencyList.indices {
                for j in fetchedList.indices {
                    if currencyList[i].code == fetchedList[j].code {
                        currencyList[i].rate = fetchedList[j].rate
                    }
                }
            }
            
            //if there are difference in lists then add these items to the end of the list
            fetchedList.forEach { fetchedItem in
                if !currencyList.contains(where: { $0.code == fetchedItem.code} ) {
                    currencyList.append(fetchedItem)
                }
            }
        }
    }
    
    func stopFetchingRates() {
        fetchTimer?.invalidate()
        fetchTimer = nil
    }
    
    func selectItem(at position: Int) {
        let removedItem = currencyList.remove(at: position)
        currencyList.insert(removedItem, at: 0)
        itemSelected?(position)
    }
}
