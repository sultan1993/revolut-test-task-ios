//
//  ConverterItemCell.swift
//  Revolut Task
//
//  Created by Sultan Seidalin on 12/7/19.
//  Copyright © 2019 Sultan Seidalin. All rights reserved.
//

import UIKit

protocol ConverterCellDelegate: class {
    func converterCell(_ cell: ConverterItemCell, didChangeCurrency currency: Currency, with amount: Float)
}

class ConverterItemCell: UITableViewCell {

    @IBOutlet private weak var flagImage: UIImageView!
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var amountField: UITextField!
    
    weak var delegate: ConverterCellDelegate?
    
    private var currency: Currency!
    private let numberFormatter = NumberFormatter()

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        amountField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }

    func configure(with currency: Currency) {
        self.currency = currency
        
        codeLabel.text = currency.code
        flagImage.image = UIImage(named: "ic_\(currency.code)_flag".lowercased())
        
        if !amountField.isFirstResponder {
            amountField.text = String(format: "%.2f", locale: Locale(identifier: "us"), currency.convertedAmount)
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if textField.isFirstResponder {
            if let text = textField.text, let number = numberFormatter.number(from: text) {
                delegate?.converterCell(self, didChangeCurrency: currency, with: number.floatValue)
            } else {
                delegate?.converterCell(self, didChangeCurrency: currency, with: 0.0)
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            amountField.becomeFirstResponder()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
}
